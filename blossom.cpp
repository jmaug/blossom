// Author: James Auger
// Date: 28th September 2016

#include "blossom.h"
#include "PerfectMatching.h"

void blossom(const int number_of_edges, const int number_of_vertices,
             const int* const edges, int* pairings) {

  // Make PM object:
  PerfectMatching pm = PerfectMatching(number_of_vertices, number_of_edges);
  
  // Add edges and weights:
  for (int i = 0; i != number_of_edges; ++i) {
    pm.AddEdge(edges[3*i], edges[3*i+1], edges[3*i+2]);
  }

  // Suppress Blossom V output:
  PerfectMatching::Options options;
  options.verbose = false;
  pm.options = options;

  // perform the matching:
  pm.Solve();

  // fetch each pairing and populate the pairings array:
  for (int u = 0, index = 0; u != number_of_vertices; u++) {
    // get vertex paired to u:
    const int v = pm.GetMatch(u);
    if (u < v) { // only add the pair if i is less than j to avoid duplicates:
      assert (index < number_of_vertices / 2);
      // store in pairings array:
      pairings[2*index] = u;
      pairings[2*index+1] = v;
      ++index;
    }
  }
}

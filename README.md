This is a Python module that allows Blossom V to be used from Python.

## What is Blossom V?

Blossom V is a fast C++ implementation of a minimum cost perfect matching algorithm written by V. Kolmogorov.
> V. Kolmogorov, Blossom v: a new implementation of a minimum cost perfect matching algorithm, Math. Prog. Comput. **1**, 43 (2009), [doi:10.1007/s12532-009-0002-8](http://dx.doi.org/10.1007/s12532-009-0002-8). 

The code can be downloaded from [here](http://pub.ist.ac.at/~vnk/software.html). **Please note:** Blossom V is free for research purposes only. Details of commercial licensing are available from the download page.

## Getting set up

Once you've downloaded the code in this repository you need to obtain the source code for Blossom V � see the details in the section titled **What is Blossom V?** for details (this code has been tested with Blossom V v2.05). Then follow these instructions:

1. Extract the Blossom V source code in the same directory as this code, e.g. your folder structure should be similar to blossom/blossom5-v2.05.src.
1. Update the makefile so that the line `BLOSSOM = blossom5-v2.05.src` has the correct name for the Blossom V source code directory.
1. Assuming cmake is installed, open a command in your code directory and type `make` to compile the Blossom V library.
1. The code should be ready to use. Try running `example.py` to check there are no problems.

## Usage

See `example.py` for an example of how to use this module.

## Todo

* Write code tests

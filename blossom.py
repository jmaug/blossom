# Module to enable Python to use Blossom V. See README.md for full details.
# Author: James Auger
# Date: 28th September 2016

from ctypes import c_int, cdll
from platform import system

# load blossom libary:
if system() == "Linux":
    _libblossom = cdll.LoadLibrary("./libblossom.so")
elif system() == "Darwin":  # aka OS X
    _libblossom = cdll.LoadLibrary("./libblossom.dylib")
else:
    raise Exception("blossom requires Linux or OS X")


def _make_c_int_array(python_list):
    """Convert a list of Python ints into a C int[] array.

    Arguments:
        python_list: list of Python ints

    Returns:
        C int[] array
    """
    array_length = len(python_list)
    c_int_array_type = c_int * array_length
    return c_int_array_type(*python_list)


class _ReverseLookup(object):
    """Class to generate lists of contiguous integers from arbitary objects."""

    def __init__(self):
        self._current_index = 0  # index counter
        self._to_index = {}  # index map
        self._from_index = []  # reverse map (normal list works fine)

    def to_index(self, u):
        """Return an integer associated with object u.

        Each new u will return a new integer counting from zero. E.g. first
        call will return 0, next call will return 1 unless u is the same for
        both calls etc.

        Arguments:
            u: object of arbitary type
        Returns:
            integer counting from zero
        """
        # If u has been used before, return same index:
        if u in self._to_index:
            return self._to_index[u]
        # Otherwise create a new index for it:
        else:
            self._to_index[u] = self._current_index
            self._from_index += [u]
            self._current_index += 1
            return self._to_index[u]

    def from_index(self, i):
        """Return object associated with index i.

        Arguments:
            i: integer index
        Returns:
            object associated with index
        """
        return self._from_index[i]

    def index_count(self):
        """Returns number of unique objects that have been indexed."""
        return self._current_index


class Matching(object):
    """Class to perform minimum weight perfect matching with Blossom V."""
    def __init__(self):
        self._edges = []  # list of edges and weights
        self._lookup = _ReverseLookup()  # to create contiguous indices

    def add_edge(self, u, v, weight):
        """Add an edge to the matching.

        Arguments:
            u: first vertex
            v: second vertex
            weight: edge weight
        """
        # Blossom V wants vertices numbered 0, 1, 2, ... so convert vertices
        # to contiguous integers:
        i = self._lookup.to_index(u)
        j = self._lookup.to_index(v)
        self._edges += [i, j, weight]

    def _create_c_arrays(self):
        """Prepare the integer arrays for C code."""
        self._c_edges = _make_c_int_array(self._edges)
        self._c_pairings = (c_int * self._number_of_vertices)()

    def _convert_c_pairings(self):
        """Convert Blossom V pairings into Python list.

        Returns:
          pairings: Python list of two-tuples representing each pairing.
        """
        number_of_pairs = self._number_of_vertices / 2

        pairings = []
        from_index = self._lookup.from_index  # 'shortcut' to function
        for i in xrange(number_of_pairs):
            u = from_index(self._c_pairings[2*i])
            v = from_index(self._c_pairings[2*i+1])
            pairings += [(u, v)]

        return pairings

    def perform_matching(self):
        """Performs Blossom V matching.

        Edges must have been added with add_edge(...).

        Returns:
          Python list of two-tuples representing each pairing.
        """
        number_of_edges = len(self._edges) / 3
        self._number_of_vertices = self._lookup.index_count()

        if self._number_of_vertices % 2 == 1:
            raise RuntimeError("Attempting to match odd number of vertices")

        # prepare C int arrays:
        self._create_c_arrays()

        # call the C++ Blossom function:
        _libblossom.blossom(number_of_edges, self._number_of_vertices,
                            self._c_edges, self._c_pairings)

        return self._convert_c_pairings()

// Author: James Auger
// Date: 28th September 2016

#ifndef BLOSSOM_H
#define BLOSSOM_H

extern "C" {
  
  // Populates pairings array with results from Blossom V matching.
  //
  // Arguments:
  //   number_of_edges: number of edges in the matching (must be even)
  //   number_of_vertices: number of vertices in the matching
  //   edges: array of edges and weights in the form 
  //          [u1, v1, w1, u2, v2, w2, ...]. This array should have 3*edges
  //          elements.
  //   pairings: empty array with space for number_of_vertices integers
  void blossom(const int number_of_edges, const int number_of_vertices,
               const int* const edges, int* pairings);

}

#endif // BLOSSOM_H
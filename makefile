BLOSSOM = blossom5-v2.05.src
BLOSSDIRS = $(BLOSSOM) $(BLOSSOM)/MinCost $(BLOSSOM)/GEOM
BLOSSSRCS = $(filter-out $(BLOSSOM)/example.cpp, $(foreach dir, $(BLOSSDIRS), $(wildcard $(dir)/*.cpp)))

SRCS = $(BLOSSSRCS) blossom.cpp
OBJS := $(SRCS:.cpp=.o)

CPPFLAGS = -I$(BLOSSOM)
CXXFLAGS = -O2
LDFLAGS = -O2
LDLIBS =
PM =

# Compile for OS X or Linux:
ifneq ($(shell uname -s), Darwin)
    LDLIBS += -lrt
    PM += libblossom.so
    LDFLAGS += -shared
    CXXFLAGS += -fpic
else
    PM += libblossom.dylib
    LDFLAGS += -dynamiclib
endif

.PHONY: all
all: $(PM)

$(PM): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS) $(LDLIBS)

%.o: %.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -c -o $@

.PHONY: clean
clean:
	rm -f $(OBJS) $(PM)
